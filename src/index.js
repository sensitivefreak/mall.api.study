'use strict';

const products = require('../products');

const Hapi = require('hapi');

const db = require('../database').db;
const Products = require('../models/products');

const server = Hapi.server({
  port: 3000,
  host: 'localhost'
});

server.route({
  method: 'GET',
  path: '/api/{collections}',
  handler: (request, h) => {
    return new Promise((resolve) => {
      let findQuery = request.params.collections == 'all' ? {} : {
        "collections": {
          '$regex': request.params.collections,
          '$options': 'i'
        }
      };
      Products.find(findQuery, function (error, items) {
        if (error) {
          console.error(error);
        }
        resolve(items);
      });
    });
  }
});

const start = async function () {
  try {
    await server.register({
      plugin: require('hapi-cors')
    });
    await server.start();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

start();