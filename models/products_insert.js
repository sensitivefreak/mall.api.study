const mongoose = require('mongoose');

const { Schema } = mongoose;

const products = require('../products');

async function run() {
  await mongoose.connect('mongodb://localhost:27017/mall', { useNewUrlParser: true });
  await mongoose.connection.dropDatabase();

  const Model = mongoose.model('products', new Schema({
    "id": Number,
    "name": String,
    "collections": String,
    "price": Number,
    "image": Array
  }));

  await Model.insertMany(products, function (err, result) {
    if (err) {
      // handle error
      console.log('error');
    } else {
      // handle success
      console.log('success');
    }
  });
}

run();