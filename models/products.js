const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var ProductSchema = new Schema({
  "id": Number,
  "name": String,
  "collections": String,
  "price": Number,
  "image": Array
});

module.exports = mongoose.model('Product', ProductSchema);

/*
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
    id: Number,
    name: String,
    collection: String,
    price: Number,
    image: [String],
    create_date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('products', productSchema);

=================================================================

var dir = './images';
var fs = require('fs');

fs.readdir(dir, function (err, fileLists) {

    let data = [];
    let dataString = '';

    fileLists.forEach(function (file, index) {

        let fileArray = file.replace('.jpg', '').split('-');
        let collection = fileArray[0].charAt(0).toUpperCase() + fileArray[0].slice(1);
        let productName = '';

        fileArray.shift();

        productName = fileArray.map(function (name) {
            return name.charAt(0).toUpperCase() + name.slice(1);
        }).join(' ');

        data.push({
            "id": index,
            "name": productName,
            "collection": collection,
            "price": (((Math.floor(Math.random() * 8) + 1) * 10) + (Math.floor(Math.random() * 8) + 1)) * 1000,
            "image": [
                file
            ]
        });

        dataString = JSON.stringify(data, null, 2);
    });

    fs.writeFile('products.json', dataString, 'utf8', function(){
        console.log('success');
    });
});

*/